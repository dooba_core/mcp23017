/* Dooba SDK
 * MCP23017 I2C I/O Expander
 */

#ifndef	__MCP23017_H
#define	__MCP23017_H

// External Includes
#include <stdint.h>
#include <avr/io.h>
#include <dio/dio.h>

// DIO Expander Structure
struct mcp23017_dio_exp
{
	// Address
	uint8_t addr;

	// DIO Expander
	struct dio_exp exp;
};

// Set Direction
extern void mcp23017_set_dir(uint8_t addr, uint16_t dir);

// Set Pull-up
extern void mcp23017_set_pullup(uint8_t addr, uint16_t pullup);

// Set GPIO
extern void mcp23017_set_gpio(uint8_t addr, uint16_t gpio);

// Get GPIO
extern uint16_t mcp23017_get_gpio(uint8_t addr);

// Register as DIO expander
extern void mcp23017_reg_dio_exp(struct mcp23017_dio_exp *e, uint8_t addr);

// DIO Expander Interface - Set Direction
extern void mcp23017_dio_set_dir(struct mcp23017_dio_exp *e, uint32_t pins);

// DIO Expander Interface - Set Pull-ups
extern void mcp23017_dio_set_pullups(struct mcp23017_dio_exp *e, uint32_t pins);

// DIO Expander Interface - Set Pins
extern void mcp23017_dio_set_pins(struct mcp23017_dio_exp *e, uint32_t pins);

// DIO Expander Interface - Get Pins
extern uint32_t mcp23017_dio_get_pins(struct mcp23017_dio_exp *e);

#endif
