/* Dooba SDK
 * MCP23017 I2C I/O Expander
 */

#ifndef	__MCP23017_REGS_H
#define	__MCP23017_REGS_H

// External Includes
#include <stdint.h>
#include <avr/io.h>

// Registers
#define	MCP23017_REG_IODIRA						0x00
#define	MCP23017_REG_IODIRB						0x01
#define	MCP23017_REG_GPPUA						0x0c
#define	MCP23017_REG_GPPUB						0x0d
#define	MCP23017_REG_GPIOA						0x12
#define	MCP23017_REG_GPIOB						0x13

#endif
