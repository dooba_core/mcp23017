/* Dooba SDK
 * MCP23017 I2C I/O Expander
 */

// External Includes
#include <string.h>
#include <i2c/i2c.h>

// Internal Includes
#include "regs.h"
#include "mcp23017.h"

// Set Direction
void mcp23017_set_dir(uint8_t addr, uint16_t dir)
{
	// Set Direction
	if(addr == 0xff)														{ return; }
	i2c_write_reg(addr, MCP23017_REG_IODIRA, dir & 0x00ff);
	i2c_write_reg(addr, MCP23017_REG_IODIRB, (dir >> 8) & 0x00ff);
}

// Set Pull-up
void mcp23017_set_pullup(uint8_t addr, uint16_t pullup)
{
	// Set Pull-up
	if(addr == 0xff)														{ return; }
	i2c_write_reg(addr, MCP23017_REG_GPPUA, pullup & 0x00ff);
	i2c_write_reg(addr, MCP23017_REG_GPPUB, (pullup >> 8) & 0x00ff);
}

// Set GPIO
void mcp23017_set_gpio(uint8_t addr, uint16_t gpio)
{
	// Write GPIO
	if(addr == 0xff)														{ return; }
	i2c_write_reg(addr, MCP23017_REG_GPIOA, gpio & 0x00ff);
	i2c_write_reg(addr, MCP23017_REG_GPIOB, (gpio >> 8) & 0x00ff);
}

// Get GPIO
uint16_t mcp23017_get_gpio(uint8_t addr)
{
	uint16_t r;

	// Read GPIO
	if(addr == 0xff)														{ return 0; }
	r = i2c_read_reg(addr, MCP23017_REG_GPIOB);
	r = (r << 8) & 0xff00;
	r = r | i2c_read_reg(addr, MCP23017_REG_GPIOA);

	return r;
}

// Register as DIO expander
void mcp23017_reg_dio_exp(struct mcp23017_dio_exp *e, uint8_t addr)
{
	// Setup Structure
	e->addr = addr;
	dio_reg_exp(&(e->exp), 16, e, (dio_exp_set_t)mcp23017_dio_set_dir, (dio_exp_set_t)mcp23017_dio_set_pullups, (dio_exp_set_t)mcp23017_dio_set_pins, (dio_exp_get_t)mcp23017_dio_get_pins);
}

// DIO Expander Interface - Set Direction
void mcp23017_dio_set_dir(struct mcp23017_dio_exp *e, uint32_t pins)
{
	// Set Direction
	mcp23017_set_dir(e->addr, ~pins);
}

// DIO Expander Interface - Set Pull-ups
void mcp23017_dio_set_pullups(struct mcp23017_dio_exp *e, uint32_t pins)
{
	// Set Pull-ups
	mcp23017_set_pullup(e->addr, pins);
}

// DIO Expander Interface - Set Pins
void mcp23017_dio_set_pins(struct mcp23017_dio_exp *e, uint32_t pins)
{
	// Set Pins
	mcp23017_set_gpio(e->addr, pins);
}

// DIO Expander Interface - Get Pins
uint32_t mcp23017_dio_get_pins(struct mcp23017_dio_exp *e)
{
	// Get Pins
	return mcp23017_get_gpio(e->addr);
}
